\babel@toc {brazil}{}
\contentsline {section}{\numberline {1}Responsabilidades do Participante}{3}{section.1}
\contentsline {section}{\numberline {2}Especifica\IeC {\c c}\IeC {\~o}es dos Rob\IeC {\^o}s}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Especifica\IeC {\c c}\IeC {\~o}es}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}}{3}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}}{3}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}}{3}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}}{3}{subsubsection.2.1.5}
\contentsline {subsection}{\numberline {2.2}Restri\IeC {\c c}\IeC {\~o}es}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}}{4}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}}{4}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}}{4}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}}{4}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}}{4}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}}{4}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}}{4}{subsubsection.2.2.7}
\contentsline {subsubsection}{\numberline {2.2.8}}{4}{subsubsection.2.2.8}
\contentsline {section}{\numberline {3}O \textit {Dohy\={o}} (Ringue de Sum\IeC {\^o})}{4}{section.3}
\contentsline {subsection}{\numberline {3.1} Interior}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Especifica\IeC {\c c}\IeC {\~o}es}{4}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}}{4}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}}{5}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Exterior do \textit {Dohy\={o}}:}{5}{subsection.3.3}
\contentsline {section}{\numberline {4}A Partida de Sum\IeC {\^o}}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}}{5}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}}{5}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}}{5}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}}{6}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}}{6}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}}{6}{subsection.4.7}
\contentsline {subsection}{\numberline {4.8}}{6}{subsection.4.8}
\contentsline {subsection}{\numberline {4.9}}{6}{subsection.4.9}
\contentsline {subsection}{\numberline {4.10}}{6}{subsection.4.10}
\contentsline {section}{\numberline {5}In\IeC {\'\i }cio, Paralisa\IeC {\c c}\IeC {\~a}o, Continua\IeC {\c c}\IeC {\~a}o e T\IeC {\'e}rmino de uma Partida}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}In\IeC {\'\i }cio:}{6}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Paralisa\IeC {\c c}\IeC {\~a}o e Continua\IeC {\c c}\IeC {\~a}o:}{6}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}T\IeC {\'e}rmino:}{7}{subsection.5.3}
\contentsline {section}{\numberline {6}Dura\IeC {\c c}\IeC {\~a}o das Partidas}{7}{section.6}
\contentsline {subsection}{\numberline {6.1}}{7}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}}{7}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}}{7}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}}{7}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}}{7}{subsubsection.6.3.2}
\contentsline {section}{\numberline {7}\textit {Yuk\IeC {\^o}}}{7}{section.7}
\contentsline {subsection}{\numberline {7.1}}{7}{subsection.7.1}
\contentsline {subsubsection}{\numberline {7.1.1}}{7}{subsubsection.7.1.1}
\contentsline {subsubsection}{\numberline {7.1.2}}{7}{subsubsection.7.1.2}
\contentsline {subsubsection}{\numberline {7.1.3}}{7}{subsubsection.7.1.3}
\contentsline {subsubsection}{\numberline {7.1.4}}{7}{subsubsection.7.1.4}
\contentsline {subsection}{\numberline {7.2}}{7}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}}{7}{subsubsection.7.2.1}
\contentsline {subsection}{\numberline {7.3}}{7}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}}{7}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}}{8}{subsubsection.7.3.2}
\contentsline {subsubsection}{\numberline {7.3.3}}{8}{subsubsection.7.3.3}
\contentsline {subsubsection}{\numberline {7.3.4}}{8}{subsubsection.7.3.4}
\contentsline {subsection}{\numberline {7.4}}{8}{subsection.7.4}
\contentsline {subsubsection}{\numberline {7.4.1}}{8}{subsubsection.7.4.1}
\contentsline {subsubsection}{\numberline {7.4.2}}{8}{subsubsection.7.4.2}
\contentsline {subsubsection}{\numberline {7.4.3}}{8}{subsubsection.7.4.3}
\contentsline {subsection}{\numberline {7.5}Viola\IeC {\c c}\IeC {\~o}es:}{8}{subsection.7.5}
\contentsline {subsection}{\numberline {7.6}Insultos:}{8}{subsection.7.6}
\contentsline {subsection}{\numberline {7.7}Viola\IeC {\c c}\IeC {\~o}es Brandas:}{8}{subsection.7.7}
\contentsline {subsubsection}{\numberline {7.7.1}}{8}{subsubsection.7.7.1}
\contentsline {paragraph}{\numberline {7.7.1.1}}{8}{paragraph.7.7.1.1}
\contentsline {paragraph}{\numberline {7.7.1.2}}{8}{paragraph.7.7.1.2}
\contentsline {subsubsection}{\numberline {7.7.2}}{8}{subsubsection.7.7.2}
\contentsline {paragraph}{\numberline {7.7.2.1}}{8}{paragraph.7.7.2.1}
\contentsline {paragraph}{\numberline {7.7.2.2}}{9}{paragraph.7.7.2.2}
\contentsline {paragraph}{\numberline {7.7.2.3}}{9}{paragraph.7.7.2.3}
\contentsline {paragraph}{\numberline {7.7.2.4}}{9}{paragraph.7.7.2.4}
\contentsline {section}{\numberline {8}Penalidades}{9}{section.8}
\contentsline {subsection}{\numberline {8.1}}{9}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}}{9}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}}{9}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}}{9}{subsection.8.4}
\contentsline {section}{\numberline {9}Les\IeC {\~o}es e Acidentes Durante a Partida}{9}{section.9}
\contentsline {subsection}{\numberline {9.1}Requisi\IeC {\c c}\IeC {\~a}o de Paralisa\IeC {\c c}\IeC {\~a}o da Partida:}{9}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Impossibilidade de Continuar a Partida:}{9}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Requisi\IeC {\c c}\IeC {\~a}o por Parte de uma Equipe para Cuidar de uma Les\IeC {\~a}o ou Acidente:}{9}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}\textit {Yuk\IeC {\^o}} Dado ao Competidor que N\IeC {\~a}o Continuar\IeC {\'a}:}{9}{subsection.9.4}
\contentsline {section}{\numberline {10}Declara\IeC {\c c}\IeC {\~a}o de Obje\IeC {\c c}\IeC {\~o}es}{10}{section.10}
\contentsline {subsection}{\numberline {10.1}}{10}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}}{10}{subsection.10.2}
\contentsline {section}{\numberline {11}Aspectos Gerais}{10}{section.11}
\contentsline {subsection}{\numberline {11.1}}{10}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}}{10}{subsection.11.2}
